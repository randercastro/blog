class Comment < ActiveRecord::Base
  belongs_to :post, dependent: :destroy
  validates :post_id, :body, presence: true
end
